## Welcome to Contributing Guide for Faces of GNOME!

List of contents:

* [Adding Dependencies](#adding-dependencies)
* [Updating Dependencies](#updating-dependencies)
* [Removing Dependencies](#removing-dependencies)

### Adding dependencies

To add new dependencies, you simply need to follow these steps:

* Install the dependency you want using `yarn`:

    ```
    yarn add bootstrap
    ```

    If you want to use specific version:

    ```
    yarn add bootstrap@4.6.1
    ```

    To add a package as dev dependency:

    ```
    yarn add eslint --dev
    ```

* Now you have the package installed locally. To use it, you will have to make a few changes:

    Head over to [`setup.sh`](./setup.sh) file, look for `dependencies` on line 41 and add your installed package.

    Example:

    ```
    dependencies=('@fortawesome' 'bootstrap' 'jquery-slim' 'moment' 'popper.js' 'slick-carousel' 'lazysizes')
    ```

    **Note**: You should only add a dependency in `setup.sh` if you wish to use that dependency within the website.

    Save this file.

* Head over to your terminal, and run setup.sh file:

    ```
    ./setup.sh
    ```
    This will install the newly added package in the `assets/3rd-party/` folder.

* Then to use that package you'll need to link the js file (e.g `/assets/3rd-party/bootstrap/dist/js/bootstrap.min.js`) in the script tags in [`/_includes/scripts.html`](./_includes/scripts.html).

### Updating Dependencies

To update the existing dependencies, you simply need to follow these steps:

* First check which packages are outdated by:

    ```
    yarn outdated
    ```
    This will give you the list of outdated packages with `Current`, `Wanted` and `Latest` versions of that package. Make sure you go for `Wanted` version.

* Next, you simply need to run:

    ```
    yarn upgrade <package@version...>
    ```

    **Note**: Make sure the newly updated packing doesn't introduce any breaking changes.

### Removing Dependencies

To delete the existing dependencies, you simply need to follow these steps:

* Delete the dependency using:

    ```
    yarn remove <package...>
    ```
    
* After the package has been removed, you need to update the list of `dependencies` on line 41 in [`setup.sh`](./setup.sh) file. Simply remove the deleted package name from the array and run:

    ```
    ./setup.sh
    ```